
var BASE_URL = 'http://ec2-34-215-210-99.us-west-2.compute.amazonaws.com';

var requestData = function(url, body, callback) {

    var request = new XMLHttpRequest();
    request.open('POST', url, true);
    request.setRequestHeader('Content-Type', 'application/json');
    // request.setRequestHeader('x-app-key', 'some-key');

    request.onload = function() {
        if (callback) {
            callback(JSON.parse(request.response));
        }
    };

    request.send(JSON.stringify(body));
};

var validKey = '';
var editMode = '';


chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        if (request.key) {
            requestData(BASE_URL + '/api/user/appKey/validate', {appKey: request.key}, function (response) {
                if (response.success) {
                    validKey = request.key;
                    chrome.runtime.sendMessage({key_valid: "yes"});
                    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                        chrome.tabs.sendMessage(tabs[0].id, {background_key: "valid"});
                    });
                } else {
                    chrome.runtime.sendMessage({key_valid: "no"});
                }
            });
        } else if (request.compare_failed) {
            validKey = ''
        } else if (request.need_key) {
            if(validKey !== '') {
                sendResponse({valid_key: validKey});
                chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                    chrome.tabs.sendMessage(tabs[0].id, {is_auth: 'yes'});
                })
            } else {
                chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                    chrome.tabs.sendMessage(tabs[0].id, {need_auth_key: 'yes'});
                });
            };
        } else if (request.edit_mode) {
            if(request.edit_mode === "on"){
                editMode = "true";
                chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                    chrome.tabs.sendMessage(tabs[0].id, {edit_mode: "on"});
                });
            } else if (request.edit_mode === "off") {
                editMode = "false";
                chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                    chrome.tabs.sendMessage(tabs[0].id, {edit_mode: "off"});
                });
            }
        } else if (request.page_load === 'yes') {
            chrome.browserAction.setIcon({path: "def-icon.png"});
            editMode = "false";
        } else if (request.need_mode) {
            sendResponse({edit_mode: editMode})
        } else if (request.change_key) {
            validKey = '';
            editMode = false;
        }
    }
);