(function () {

        var SmartBar = function () {
            __self = this;
            this.editMode = false;
            this.storage = chrome.storage;
            this.avoidDates = [];
            this.frontierDates = {
                startTime: [],
                endTime: []
            };
            this.expName = '';
            this.currentExpFromDate = '';
            this.currentExpToDate = '';
            this.currentExpId = '';
        };
        
        SmartBar.prototype.PopupFormatDate = function (date) {

            //FOR FORMATTING CURRENT EXPERIMENTS DATE

                var monthNames = [
                    "Jan", "Feb", "Mar",
                    "Apr", "May", "Jun", "Jul",
                    "Aug", "Sep", "Oct",
                    "Nov", "Dec"
                ];

                var day = date.getDate();
                var monthIndex = date.getMonth();

                return moment(date).format('MM/DD/YYYY HH:mm a');

                // return day + ' ' + monthNames[monthIndex];
        };

        SmartBar.prototype.PopupOnLoadListeners = function () {

            //FOR FETCHING AUTHENTICATION, KEY, EDIT MODE

            var that = this;

            window.addEventListener('load', function () {
                var row = document.querySelector('.row');
                var form = document.querySelector('.enter-form');
                var enterFormError = document.querySelector('.enter-form__error');
                var formHeader = document.querySelector('.enter-form__header');
                var expForm = document.querySelector('.content__experiment-form');
                var authSuccess = document.querySelector('.content-auth-success');
                var createExpBtn = document.getElementById('content__create_experiment');
                var currExp = document.querySelector('.content__current-experiment');
                var tabs = document.getElementById('content-tabs');
                var changeKeyBtn = document.getElementById('change_key');
                var headerNav = document.querySelector('.content__navigation');

                tabs.style.display = "none";
                tippy(changeKeyBtn);
                // changeKeyBtn.style.display = 'none';

                chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                    chrome.tabs.sendMessage(tabs[0].id, {need_show: true});
                });

                chrome.runtime.onMessage.addListener(
                    function(request, sender, sendResponse) {
                        if(request.to_show === true) {
                            document.getElementById('available').style.display = 'block';
                            document.querySelector('.unavailable').style.display = 'none';
                        } else if (request.is_auth) {
                            headerNav.style.display = 'block';
                            form.style.display = 'none';
                            formHeader.style.display = 'none';
                            expForm.style.display = 'none';
                            enterFormError.style.display = 'none';
                            authSuccess.style.display = "block";
                            row.style.marginBottom = '0';
                            createExpBtn.style.display = "block";
                            tabs.style.display = "block";
                        }
                    }
                );

                chrome.runtime.sendMessage({need_key: true}, function (response) {
                    if(response.valid_key !== '') {
                        headerNav.style.display = 'block';
                        form.style.display = 'none';
                        formHeader.style.display = 'none';
                        enterFormError.style.display = 'none';
                        authSuccess.style.display = "block";
                        row.style.marginBottom = '0';
                        createExpBtn.style.display = "block";
                        tabs.style.display = "block";
                        // createExpBtn.style.display = 'none';
                        // tabs.style.display = "block";
                    }
                });

                chrome.runtime.sendMessage({need_mode: true}, function (response) {
                    if(response.edit_mode === "true") {
                        that.editMode = true;
                        that.PopupEditModeOn();
                        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                            chrome.tabs.sendMessage(tabs[0].id, {edit_mode: "on"});
                        });
                    } else if (response.edit_mode === "false"){
                        that.editMode = false;
                        that.PopupEditModeOff();
                        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                            chrome.tabs.sendMessage(tabs[0].id, {edit_mode: "off"});
                        });
                    }
                });

                // chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                //     chrome.tabs.sendMessage(tabs[0].id, {need_button: true}, function (response) {
                //         if (response.should_show_button === "yes") {
                //             var editButton = document.getElementById('edit_mode');
                //             var formSuc = document.querySelector('.exp_form-suc');
                //             editButton.style.display = 'block';
                //             formSuc.style.display = 'block';
                //             currExp.style.display = 'block';
                //             currExp.innerHTML = 'Current Experiment </br>' + response.current_experiment;
                //         }
                //     });
                // });
            });
        };

        SmartBar.prototype.PopupFormValidate = function () {

            //FOR FORM VALIDATING, FETCHING VALID KEY AND COMPARING

            var row = document.querySelector('.row');
            var form = document.querySelector('.available .enter-form');
            var key = document.querySelector('input[id=key]');
            var enterFormError = document.querySelector('.enter-form__error');
            var formHeader = document.querySelector('.enter-form__header');
            var authSuccess = document.querySelector('.content-auth-success');
            var createExpBtn = document.getElementById('content__create_experiment');
            var popupTabs = document.getElementById('content-tabs');
            var changeKeyBtn = document.getElementById('change_key');
            var content = document.querySelector('.content');
            var preloader = document.querySelector('.experiments-preloader');
            var formSucMsg = document.querySelector('.experiment-form__success');
            var expForm = document.querySelector('.content__experiment-form');
            var headerNav = document.querySelector('.content__navigation');



            form.onsubmit = function (e) {
                e.preventDefault();
                chrome.runtime.sendMessage({key: key.value});
            };
            chrome.runtime.onMessage.addListener(
                function(request, sender, sendResponse) {
                    if(request.key_valid === "yes") {
                        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                            chrome.tabs.sendMessage(tabs[0].id, {content_valid_key: key.value}, function(response) {
                                if (response.compare === 'false') {
                                    enterFormError.style.display = 'block';
                                    enterFormError.innerHTML = "This is not actual key";
                                    enterFormError.style.color = "#D32F2F";
                                    chrome.runtime.sendMessage({compare_failed: 'true'});
                                    return

                                } else if(response.compare === 'true') {
                                    headerNav.style.display = 'block';
                                    form.style.display = 'none';
                                    formHeader.style.display = 'none';
                                    enterFormError.style.display = 'none';
                                    content.style.display = 'block';
                                    expForm.style.display = 'none';
                                    authSuccess.style.display = "block";
                                    row.style.marginBottom = '0';
                                    createExpBtn.style.display = "block";
                                    preloader.style.display = 'inline';
                                    popupTabs.style.display = "block";
                                    formSucMsg.style.display = 'none';
                                }
                            });
                        });

                    } else if (request.key_valid === "no") {
                        enterFormError.style.display = 'block';
                        enterFormError.innerHTML = "Keys doesn`t match";
                        enterFormError.style.color = "#D32F2F";
                    }
                }
            );
        };

        SmartBar.prototype.PopupFetchPendingExperiments = function () {

            // FOR FETCHING NOT ACTUAL EXPERIMENTS AND DISPLAYING IT

            var that = this;
            var container = document.querySelector('.content__experiments-collection');
            var experimentsPreloader = document.querySelector('.experiments-preloader');
            var refetchingText = document.getElementById('content__experiments-refetch');
            var createExpBtn = document.getElementById('content__create_experiment');
            var formSucMsg = document.querySelector('.experiment-form__success');
            var collectionHeader = document.querySelector('.content__experiments-header');

            chrome.runtime.onMessage.addListener(
                function(request, sender, sendResponse) {
                    if (document.querySelector('.enter-form').style.display === 'none') {
                        if (request.completedExperiments) {
                            experimentsPreloader.style.display = 'none';
                            refetchingText.style.display = 'none';
                            container.style.display = 'block';

                            var completedExperiment = document.createElement('a');
                            var completedExperimentTime = document.createElement('p');
                            var completedExperimentStatus = document.createElement('p');

                            completedExperiment.classList.add('collection-item');
                            completedExperiment.classList.add('completed_experiments');
                            completedExperiment.classList.add('grey-text');
                            completedExperiment.classList.add('text-darken-1');
                            completedExperiment.style.paddingLeft = '10px';
                            completedExperiment.style.paddingRight = '5px';
                            completedExperiment.style.textAlign = 'left';

                            completedExperiment.id = request.completedExperiments._id;
                            completedExperiment.innerHTML = request.completedExperiments.name;
                            completedExperiment.style.fontWeight = 'bold';
                            completedExperiment.style.backgroundColor = '#FAFAFA';
                            completedExperimentTime.innerHTML = 'From ' + that.PopupFormatDate(new Date(request.completedExperiments.start_time)) + ' To ' + that.PopupFormatDate(new Date(request.completedExperiments.end_time));
                            completedExperimentStatus.innerHTML = 'Complete';
                            completedExperimentTime.style.fontWeight = 'normal';
                            completedExperimentStatus.style.fontWeight = 'normal';
                            completedExperimentTime.style.margin = 0;
                            completedExperimentStatus.style.margin = 0;

                            completedExperiment.appendChild(completedExperimentTime);
                            completedExperiment.appendChild(completedExperimentStatus);

                            container.append(completedExperiment);
                            }

                            if (request.pendingExperiment) {

                                if (request.pendingExperiment === 'false') {
                                    createExpBtn.style.display = 'block';
                                    experimentsPreloader.style.display = 'none';
                                    collectionHeader.innerHTML = 'No experiments';
                                    refetchingText.style.display = 'none';
                                    return
                                }

                                formSucMsg.style.display = 'none';
                                createExpBtn.style.display = 'none';
                                experimentsPreloader.style.display = 'none';
                                refetchingText.style.display = 'none';
                                container.style.display = 'block';

                                if(document.querySelector('.current-experiment')){
                                    return
                                }

                                var experiment = document.createElement('a');
                                var editContentOption = document.createElement('div');
                                var editContentButton = document.createElement('a');
                                var previewButton = document.createElement('a');
                                var editButton = document.createElement('a');
                                var deleteButton = document.createElement('a');
                                var experimentTime = document.createElement('p');
                                var experimentStatus = document.createElement('p');
                                var experimentOptions = document.createElement('div');
                                var currExp = document.querySelector('.content__current-experiment');
                                {/*<button id="edit_mode" class="btn waves-effect waves-light brown">*/
                                }
                                {/*<span>Edit off</span>*/
                                }
                                {/*</button>*/
                                }
                                editContentButton.id = 'edit_mode';
                                editContentButton.classList.add('btn');
                                editContentButton.classList.add('waves-effect');
                                editContentButton.classList.add('waves-light');
                                editContentButton.style.marginLeft = '20px';
                                editContentButton.style.marginTop = '10px';

                                if (that.editMode === false) {
                                    editContentButton.innerHTML = 'OFF';
                                    editContentButton.classList.remove('green');
                                    editContentButton.classList.add('brown');
                                }

                                if (that.editMode === true) {
                                    editContentButton.innerHTML = 'ON';
                                    editContentButton.classList.remove('brown');
                                    editContentButton.classList.add('green');
                                }


                                experiment.classList.add('grey-text');
                                experiment.classList.add('text-darken-1');
                                experiment.classList.add('current-experiment');

                                experiment.style.textAlign = 'center';


                                experiment.id = request.pendingExperiment._id;

                                if (!request.pendingExperiment.name) {
                                    that.expName = 'No name';
                                } else {
                                    that.expName = request.pendingExperiment.name;
                                }

                                currExp.style.display = 'block';

                                experiment.innerHTML = that.expName;
                                experiment.style.fontWeight = 'bold';
                                experiment.style.textAlign = 'center';

                                experimentTime.innerHTML = 'From ' + that.PopupFormatDate(new Date(request.pendingExperiment.start_time)) + ' To ' + that.PopupFormatDate(new Date(request.pendingExperiment.end_time))
                                experimentStatus.innerHTML = 'Status: ' + request.pendingExperiment.status.charAt(0).toUpperCase() + request.pendingExperiment.status.slice(1);
                                experimentTime.style.fontWeight = 'normal';
                                experimentStatus.style.fontWeight = 'normal';

                                experimentTime.classList.add('experiment-time');
                                // badge.classList.add('new');
                                // badge.classList.add('badge');
                                // badge.classList.add('blue');
                                // badge.classList.add('darken-1');
                                // badge.id = request.pendingExperiment._id;
                                // badge.setAttribute('data-badge-caption', 'Preview');
                                // badge['data-name'] = that.expName;

                                previewButton.id = request.pendingExperiment._id;
                                previewButton.classList.add('waves-effect');
                                previewButton.classList.add('waves-light');
                                previewButton.classList.add('preview-experiment');
                                previewButton.classList.add('btn');
                                previewButton.classList.add('blue');
                                previewButton.classList.add('darken-1');
                                previewButton.innerHTML = 'Preview';
                                previewButton.style.marginRight = '15px';
                                previewButton.style.fontWeight = 'normal';



                                editButton.classList.add('waves-effect');
                                editButton.classList.add('waves-light');
                                editButton.classList.add('btn');
                                editButton.classList.add('edit-badge');
                                editButton.classList.add('brown');
                                editButton.style.fontWeight = 'normal';
                                editButton.style.width = '100px';
                                editButton.style.marginRight = '15px';
                                editButton.style.marginLeft = '15px';



                                editButton['data-id'] = request.pendingExperiment._id;
                                editButton['data-name'] = that.expName;
                                editButton['data-start-time'] = request.pendingExperiment.start_time;
                                editButton['data-end-time'] = request.pendingExperiment.end_time;


                                editButton.innerHTML = 'EDIT';
                                editButton['data-id'] = request.pendingExperiment._id;
                                editButton['data-name'] = that.expName;
                                editButton['data-start-time'] = request.pendingExperiment.start_time;
                                editButton['data-end-time'] = request.pendingExperiment.end_time;

                                deleteButton.classList.add('waves-effect');
                                deleteButton.classList.add('waves-light');
                                deleteButton.classList.add('btn');
                                deleteButton.classList.add('delete-badge');
                                deleteButton.classList.add('red');
                                deleteButton.classList.add('darken-1');
                                deleteButton.style.fontWeight = 'normal';
                                deleteButton.style.width = '100px';
                                deleteButton.style.marginLeft = '15px';


                                deleteButton.innerHTML = 'DELETE';
                                deleteButton['data-id'] = request.pendingExperiment._id;
                                deleteButton['data-name'] = request.pendingExperiment.name;

                                experimentTime.style.margin = 0;
                                experimentTime.style.textAlign = 'center';
                                experimentTime.style.fontSize = '14px';

                                experimentStatus.style.margin = 0;
                                experimentStatus.style.textAlign = 'center';
                                experimentStatus.style.fontSize = '14px';


                                editContentOption.innerHTML = 'Change Content';
                                editContentOption.style.marginTop = '20px';
                                editContentOption.style.fontWeight = 'normal';
                                editContentOption.style.fontSize = '14px';


                                editContentOption.append(editContentButton);

                                experiment.appendChild(experimentTime);
                                experiment.appendChild(experimentStatus);

                                experimentOptions.style.marginTop = '10px';
                                experimentOptions.appendChild(previewButton);
                                experimentOptions.appendChild(editButton);
                                experimentOptions.appendChild(deleteButton);

                                experiment.appendChild(editContentOption);
                                experiment.appendChild(experimentOptions);


                                if (new Date() >= new Date(request.pendingExperiment.start_time)) {
                                    if (new Date() <= new Date(request.pendingExperiment.end_time)) {
                                        editButton['data-current'] = 'true';
                                        editButton.classList.add('disabled');
                                        previewButton.classList.add('disabled');
                                        previewButton.classList.remove('darken-1');
                                        deleteButton.classList.add('delete-actual-experiment');
                                        experimentStatus.innerHTML = 'Status: Live';
                                        editContentButton.classList.add('disabled');
                                    }
                                }
                                currExp.innerHTML = '';
                                currExp.prepend(experiment);
                                that.avoidDates.push(
                                    moment.range(
                                        moment(request.pendingExperiment.start_time).add(1, 'days'),
                                        moment(request.pendingExperiment.end_time).subtract(1, 'days')
                                    )
                                );
                                that.frontierDates.startTime.push(request.pendingExperiment.start_time);
                                that.frontierDates.endTime.push(request.pendingExperiment.end_time);
                        }
                    }
                });
        };

        SmartBar.prototype.PopupCreateNewExperiment = function () {

            //FOR CREATING NEW EXPERIMENT, VALIDATE DATE AND SEND THIS DATA TO CONTENT FOR ACTUAL CREATING

            var createExpBtn = document.getElementById('content__create_experiment');
            var ExperimentForm = document.querySelector('.content__experiment-form');
            var saveButton = document.getElementById('experiment-form__save');
            var newExp = document.getElementById('experiment-form__create');
            var fromDate = document.getElementById('experiment-form__from');
            var toDate = document.getElementById('experiment-form__to');
            var expName = document.getElementById('experiment-form__name');

            var formSucMsg = document.querySelector('.experiment-form__success');
            var currExp = document.querySelector('.content__current-experiment');
            var cancelButton = document.getElementById('experiment-form__cancel');
            var nameLabel = document.querySelector('label[for=experiment-form__name]');
            var startLabel = document.querySelector('label[for=experiment-form__from]');
            var formError = document.querySelector('.experiment-form__error');

            formSucMsg.style.color = '#0e83cd';
            formSucMsg.style.display = 'none';

            formSucMsg.style.textAlign = 'center';
            formSucMsg.style.margin = '15px auto';
            formSucMsg.style.fontSize = '18px';

            var that = this;


            createExpBtn.addEventListener('click', function () {
                function getDates(startDate, stopDate) {
                    var dateArray = [];
                    var currentDate = moment(startDate);
                    var stopDate = moment(stopDate);
                    while (currentDate <= stopDate) {
                        dateArray.push( moment(currentDate).format('YYYY-MM-DD'));
                        currentDate = moment(currentDate).add(1, 'days');
                    }
                    return dateArray;
                }

                function allAvoidDates() {
                   return that.avoidDates.map(function (date) {
                        return getDates(date.start, date.end)
                    })
                }

                var allDates = [].concat.apply([], allAvoidDates());
                that.avoidDates = allDates;

                this.style.display = 'none';
                ExperimentForm.style.display = 'block';
                saveButton.style.display = 'none';
                newExp.style.display = 'block';
                cancelButton.style.display = 'none';
                expName.style.display = 'block';
                nameLabel.style.display = 'block';
                fromDate.style.display = 'block';
                startLabel.style.display = 'block';
                formError.style.display = 'none';

                expName.value = '';
                fromDate.value = '';
                toDate.value = '';


                $('#experiment-form__from').datetimepicker({ minDate: moment(), showClose: true });
                $('#experiment-form__to').datetimepicker({ minDate: moment(), showClose: true });

                $('#experiment-form__from').data("DateTimePicker").disabledDates(allDates.map(function (date) {
                    return new Date(date)
                }));
                $('#experiment-form__to').data("DateTimePicker").disabledDates(allDates.map(function (date) {
                    return new Date(date)
                }));


            });

            newExp.addEventListener('click', function () {

                var timePassed = true;


                function dateDiffInDays(a, b) {
                    var _MS_PER_DAY = 1000 * 60 * 60 * 24;
                    var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
                    var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
                    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
                }

                for(var i = 0; i < that.frontierDates.endTime.length; i++) {
                    for(var j = 0; j < that.frontierDates.startTime.length; j++) {


                        if(dateDiffInDays(new Date(fromDate.value), new Date(that.frontierDates.endTime[i])) !== 0
                            &&
                            dateDiffInDays(new Date(toDate.value), new Date(that.frontierDates.startTime[j])) !== 0
                            &&
                            (dateDiffInDays(new Date(toDate.value), new Date(that.frontierDates.startTime[j])) !== 0 && dateDiffInDays(new Date(fromDate.value), new Date(that.frontierDates.endTime[j])) !== 0)
                        ) {
                            if(new Date(fromDate.value) < new Date(that.frontierDates.startTime[j]) && new Date(toDate.value) > new Date(that.frontierDates.endTime[j])){
                                timePassed = false;
                            }
                            continue;
                        }

                        if (dateDiffInDays(new Date(fromDate.value), new Date(that.frontierDates.endTime[i])) === 0) {
                            if (new Date(fromDate.value) < new Date(that.frontierDates.endTime[i])) {
                                 timePassed = false;
                            }
                        }
                        //passed

                        if(dateDiffInDays(new Date(toDate.value), new Date(that.frontierDates.startTime[j])) === 0) {
                            if(new Date(toDate.value) > new Date(that.frontierDates.startTime[j])) {
                                 timePassed = false;
                            }
                        }
                        //passed




                        if(dateDiffInDays(new Date(toDate.value), new Date(that.frontierDates.startTime[j])) === 0 && dateDiffInDays(new Date(fromDate.value), new Date(that.frontierDates.endTime[j])) === 0) {
                            if (new Date(fromDate.value) > new Date(that.frontierDates.startTime[j]) && new Date(toDate.value) < new Date(that.frontierDates.endTime[j])
                            ) {
                                timePassed = false;
                            } else if (new Date(toDate.value) > new Date(that.frontierDates.endTime[j]) && new Date(fromDate.value) > new Date(that.frontierDates.endTime[j])
                                ||
                                new Date(toDate.value) < new Date(that.frontierDates.startTime[j]) && new Date(fromDate.value) < new Date(that.frontierDates.startTime[j])) {
                                timePassed = true;
                            }
                        }
                    }
                }

                if (expName.value !== '' && fromDate.value !== '' && toDate.value !== '') {

                    if(!/^\S*$/.test(expName.value.trim())){
                        formError.style.display = 'block';
                        return formError.innerHTML = "Please do not use spaces in the name field";
                    }
                    if(new Date(toDate.value) < new Date(fromDate.value)) {
                        formError.style.display = 'block';
                        return formError.innerHTML = "Invalid Date. End Date can not be less than the Start Date";
                    }

                    function getUserDates(startDate, stopDate) {
                        var dateArray = [];
                        var currentDate = moment(startDate);
                        var stopDate = moment(stopDate);
                        while (currentDate <= stopDate) {
                            dateArray.push( moment(currentDate).format('YYYY-MM-DD'));
                            currentDate = moment(currentDate).add(1, 'days');
                        }
                        return dateArray;
                    }

                    for(var k = 0; k < that.avoidDates.length; k++) {
                        if(getUserDates(fromDate.value, toDate.value).includes(that.avoidDates[k])){
                            formError.style.display = 'block';
                            formError.innerHTML = "The time interval is not available. We found a different experiment starting or ending between this time interval";
                            return ;
                        }
                    }

                    if(!timePassed) {
                        formError.style.display = 'block';
                        formError.innerHTML = 'The time interval is not available. We found a different experiment starting or ending between this time interval';
                        return ;
                    }

                    var collectionHeader = document.querySelector('.content__experiments-header');

                    that.expName = expName.value;
                    createExpBtn.style.display = 'none';
                    ExperimentForm.style.display = 'none';
                    formSucMsg.innerHTML = 'Successfully created!';
                    currExp.style.display = 'block';
                    currExp.innerHTML = 'Waiting...';
                    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                        chrome.tabs.sendMessage(tabs[0].id, {exp_name: expName.value.trim(), from_date: fromDate.value, to_date: toDate.value});
                    });

                    collectionHeader.innerHTML = 'All Experiments';
                    that.PopupEditModeOn();


                } else {
                    formError.style.display = 'block';
                    formError.innerHTML = "Please check all fields";
                }
            })
        };

        SmartBar.prototype.PopupEditModeOn = function() {

            // FOR ENABLE EDIT MODE

            this.editMode = true;
            chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                chrome.tabs.sendMessage(tabs[0].id, {edit_mode: "on"});
            });
            chrome.runtime.sendMessage({edit_mode: "on"});
            chrome.browserAction.setIcon({path: "active-icon.png"});
            var editButton = document.getElementById('edit_mode');
            if (editButton) {
                editButton.innerHTML = 'On';
                editButton.classList.remove('brown');
                editButton.classList.add('green');
            }
        };

        SmartBar.prototype.PopupEditModeOff = function() {

            //FOR DISABLE EDIT MODE

            this.editMode = false;
            chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                chrome.tabs.sendMessage(tabs[0].id, {edit_mode: "off"});
            });
            chrome.runtime.sendMessage({edit_mode: "off"});
            chrome.browserAction.setIcon({path: "def-icon.png"});
            var editButton = document.getElementById('edit_mode');
            if (editButton) {
                editButton.innerHTML = 'Off';
                editButton.classList.remove('green');
                editButton.classList.add('brown');
            }
        };

        SmartBar.prototype.PopupChangeEditMode = function() {

            //FOR CHANGING EDIT MODE

            // var formSucMsg = document.querySelector('.exp_form-suc-msg');
            // formSucMsg.style.display = 'none';
            if (this.editMode) {
                this.PopupEditModeOff();
            } else {
                this.PopupEditModeOn();
            }
        };


        SmartBar.prototype.PopupHandleEditMode = function () {

            // EVENT LISTENER FOR CHANGING EDIT MODE

            // var editButton = document.getElementById('edit_mode');

            $(document).on('click', '#edit_mode', this.PopupChangeEditMode.bind(this));
            // editButton.addEventListener('click', this.PopupChangeEditMode.bind(this));
        };

        SmartBar.prototype.PopupPreviewExperiment = function () {

            // FOR ENABLING CURRENT EXPERIMENT
            var that = this;
            $(document).on('click', '.preview-experiment', function() {
                var experimentID = this.id;
                var formSucMsg = document.querySelector('.experiment-form__success');

                formSucMsg.style.display = 'none';
                chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                    chrome.tabs.sendMessage(tabs[0].id, {preview_exp_ID: experimentID});
                });
            });
        };

        SmartBar.prototype.PopupEditExperiment = function () {

            // FOR EDITING EXPERIMENT, VALIDATE DATE AND SEND THIS DATA TO CONTENT

            var that = this;
            var rangeDates = [];


            var createExpBtn = document.getElementById('content__create_experiment');
            var newExpButton = document.getElementById('experiment-form__create');
            var saveButton = document.getElementById('experiment-form__save');
            var cancelButton = document.getElementById('experiment-form__cancel');

            var formSucMsg = document.querySelector('.experiment-form__success');
            var currExp = document.querySelector('.content__current-experiment');

            var ExperimentForm = document.querySelector('.content__experiment-form');
            var fromDate = document.getElementById('experiment-form__from');
            var toDate = document.getElementById('experiment-form__to');
            var expName = document.getElementById('experiment-form__name');
            var nameLabel = document.querySelector('label[for=experiment-form__name]');
            var startLabel = document.querySelector('label[for=experiment-form__from]');
            var endLabel = document.querySelector('label[for=experiment-form__to]');
            var formError = document.querySelector('.experiment-form__error');

            $(document).on('click', '.edit-badge', function() {
                ExperimentForm.style.display = 'block';
                newExpButton.style.display = 'none';
                createExpBtn.style.display = 'none';
                formSucMsg.style.display = 'none';
                currExp.style.display = 'none';
                cancelButton.style.display = 'inline';
                nameLabel.classList.add('active');
                startLabel.classList.add('active');
                endLabel.classList.add('active');
                formError.style.display = 'none';
                saveButton.style.display = 'inline';

                if(this['data-current'] === 'true') {
                    expName.style.display = 'none';
                    nameLabel.style.display = 'none';
                    fromDate.style.display = 'none';
                    startLabel.style.display = 'none';
                }

                that.currentExpFromDate = moment(this['data-start-time']).format('MM/DD/YYYY hh:mm A');
                that.currentExpToDate = moment(this['data-end-time']).format('MM/DD/YYYY hh:mm A');
                expName.value = this['data-name'];
                fromDate.value = that.currentExpFromDate;
                toDate.value = that.currentExpToDate;
                that.currentExpId = this['data-id'];


                function getDates(startDate, stopDate) {
                    var dateArray = [];
                    var currentDate = moment(startDate);
                    var stopDate = moment(stopDate);
                    while (currentDate <= stopDate) {
                        dateArray.push( moment(currentDate).format('YYYY-MM-DD'));
                        currentDate = moment(currentDate).add(1, 'days');
                    }
                    return dateArray;
                }

                var currentExpDatesRange = getDates(this['data-start-time'], this['data-end-time']);

                function allAvoidDates() {
                    return that.avoidDates.map(function (date) {
                        return getDates(date.start, date.end)
                    })
                }

                var allDates = [].concat.apply([], allAvoidDates());

                for(var j = 0; j < allDates.length; j++) {
                    if((currentExpDatesRange.includes(allDates[j]))) {
                         allDates = allDates.filter(function(val) {
                             return currentExpDatesRange.indexOf(val) === -1;
                        });
                    }
                }

                rangeDates = allDates;

                $('#experiment-form__from').datetimepicker({minDate: moment().subtract(3, 'hours'), showClose: true});
                $('#experiment-form__to').datetimepicker({minDate: moment().subtract(3, 'hours'), showClose: true});

                $('#experiment-form__from').data("DateTimePicker").disabledDates(allDates.map(function (date) {
                    return new Date(date)
                }));
                $('#experiment-form__to').data("DateTimePicker").disabledDates(allDates.map(function (date) {
                    return new Date(date)
                }));
            });

            cancelButton.addEventListener('click', function () {
                ExperimentForm.style.display = 'none';
                currExp.style.display = 'block';
                saveButton.style.display = 'inline';
            });

            saveButton.addEventListener('click', function () {
                var fromDate = document.getElementById('experiment-form__from');
                var toDate = document.getElementById('experiment-form__to');
                var expName = document.getElementById('experiment-form__name');

                if (expName.value !== '' && fromDate.value !== '' && toDate.value !== '') {
                    if(!/^\S*$/.test(expName.value.trim())){
                        formError.style.display = 'block';
                        return formError.innerHTML = "Please do not use spaces in the name field";
                    }
                    if(new Date(toDate.value) < new Date(fromDate.value)) {
                        formError.style.display = 'block';
                        return formError.innerHTML = "Invalid Date. End Date can not be less than the Start Date";
                    }
                    function getUserDates(startDate, stopDate) {
                        var dateArray = [];
                        var currentDate = moment(startDate);
                        var stopDate = moment(stopDate);
                        while (currentDate <= stopDate) {
                            dateArray.push( moment(currentDate).format('YYYY-MM-DD'));
                            currentDate = moment(currentDate).add(1, 'days');
                        }
                        return dateArray;
                    }

                    for(var j = 0; j < rangeDates.length; j++) {
                        if(getUserDates(fromDate.value, toDate.value).includes(rangeDates[j])){
                            chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                                chrome.tabs.sendMessage(tabs[0].id, {avoid_dates: rangeDates});
                            });
                            formError.style.display = 'block';
                            formError.innerHTML = "The time interval is not available. We found a different experiment starting or ending between this time interval";
                            return ;
                        }
                    }
                    that.expName = expName.value;
                    that.avoidDates.length = 0;
                    that.frontierDates = {
                        startTime: [],
                        endTime: []
                    };


                    ExperimentForm.style.display = 'none';
                    currExp.style.display = 'block';
                    currExp.innerHTML = 'Waiting...';

                    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                        chrome.tabs.sendMessage(tabs[0].id, {update_exp_ID: that.currentExpId, update_exp_name: expName.value.trim(), update_from_date: fromDate.value, update_to_date: toDate.value});
                    });

                    that.PopupEditModeOff();

                } else {
                    formError.style.display = 'block';
                    formError.innerHTML = "Please check all fields";
                }
            })
        };

        SmartBar.prototype.PopupDeleteExperiment = function () {

            //FOR DELETING EXPERIMENT FROM UI AND SEND EXPERIMENT DATA TO CONTENT FOR ACTUAL DELETING

            var that = this;

            $(document).on('click', '.delete-badge', function() {
                var experimentID = this['data-id'];
                var formSucMsg = document.querySelector('.experiment-form__success');
                var currExp = document.querySelector('.content__current-experiment');
                var createExpBtn = document.getElementById('content__create_experiment');


                formSucMsg.style.display = 'block';
                formSucMsg.innerHTML = 'Successfully deleted!';
                that.expName = '';
                that.avoidDates.length = 0;
                that.frontierDates = {
                    startTime: [],
                    endTime: []
                };

                that.PopupEditModeOff();
                currExp.innerHTML = '';
                createExpBtn.style.display = 'block';

                chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                    chrome.tabs.sendMessage(tabs[0].id, {delete_experiment_ID: experimentID});
                });

                chrome.runtime.sendMessage({edit_mode: "off"});

                that.PopupEditModeOff();

            });
        };

        SmartBar.prototype.PopupChangeAppKey = function () {
          var changeKeyBtn = document.getElementById('change_key');
          var that = this;
          changeKeyBtn.addEventListener('click', function () {
              var content = document.querySelector('.content');
              var enterFormWrap = document.querySelector('.enter-form__wrapper');
              var enterFormHeader = document.querySelector('.enter-form__header');
              var enterForm = document.querySelector('.enter-form');


              content.style.display = 'none';
              enterForm.style.display = 'block';
              enterFormWrap.style.display = 'block';
              enterFormHeader.style.display = 'block';
              this.style.display = 'none';
              $("#content__experiments-collection a").remove();
              $(".content__current-experiment").html('');

              chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                  chrome.tabs.sendMessage(tabs[0].id, {change_key: 'true'});
              });
              chrome.runtime.sendMessage({change_key: 'true'});
              that.PopupEditModeOff()
          });
        };

        SmartBar.prototype.PopupFetchAnalytics = function () {
            var that = this;
            document.getElementById('total-sessions-tab').classList.add('active');
            $(document).on('click', '#analytics_tab', function() {
                var analyticsContent = document.getElementById('analytics__content');
                if(analyticsContent.style.display === 'block'){
                    return
                }
                else {
                    analyticsContent.style.display = 'block';
                    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                        chrome.tabs.sendMessage(tabs[0].id, {fetch_analytics: true});
                    });
                    chrome.runtime.onMessage.addListener(
                        function(request, sender, sendResponse) {
                            if(request.analytics) {
                                that.PopupTotalChart(request);
                                that.PopupAbandonedChart(request);
                                that.PopupContinuedChart(request)
                            }
                        }
                    );
                }
            })
        };

        SmartBar.prototype.PopupTotalChart = function (request) {

            var totalBadge = document.querySelector('.total-sessions-badge-total');
            var abandonedBadge = document.querySelector('.total-sessions-badge-abandoned');
            var continuedBadge = document.querySelector('.total-sessions-badge-continued');

            totalBadge.innerHTML = request.analytics.sessions;
            abandonedBadge.innerHTML = request.analytics.abandoned.count;
            continuedBadge.innerHTML = request.analytics.continued.count;


            var chart = new CanvasJS.Chart("total-sessions-chart",
                {
                    theme: "theme1",
                    data: [
                        {
                            type: "doughnut",
                            showInLegend: true,
                            toolTipContent: '{name}',
                            legendText: "{name}",
                            legend:{
                                fontSize: 36,
                            },
                            dataPoints: [
                                {
                                    y: request.analytics.continued.count,
                                    reachedFooter: request.analytics.continued.reachedFooter,
                                    notReachedFooter: request.analytics.continued.notReachedFooter,
                                    // indexLabel: "Continued",
                                    name: "Retention"
                                },
                                {
                                    y: request.analytics.abandoned.count,
                                    reachedFooter: request.analytics.abandoned.reachedFooter,
                                    notReachedFooter: request.analytics.abandoned.notReachedFooter,
                                    // indexLabel: "Abandoned",
                                    name: "Bounced"
                                }
                            ]
                        }
                    ]
                });
            chart.render();
        };

        SmartBar.prototype.PopupAbandonedChart = function (request) {
                var totalBadge = document.querySelector('.abandoned-sessions-badge-total');
                var reachedBadge = document.querySelector('.abandoned-sessions-badge-reached');
                var skippedBadge = document.querySelector('.abandoned-sessions-badge-skipped');

                totalBadge.innerHTML = request.analytics.abandoned.count;
                reachedBadge.innerHTML = request.analytics.abandoned.reachedFooter;
                skippedBadge.innerHTML = request.analytics.abandoned.notReachedFooter;

                if(request.analytics.abandoned.reachedFooter === 0 && request.analytics.abandoned.notReachedFooter === 0){
                    document.getElementById('abandoned-sessions-chart').innerHTML = 'No analytics for bounced sessions';
                    return
                }

                var chart = new CanvasJS.Chart("abandoned-sessions-chart",
                    {
                        theme: "theme1",
                        data: [
                            {
                                type: "doughnut",
                                showInLegend: true,
                                toolTipContent: '{name}',
                                legendText: "{name}",
                                dataPoints: [
                                    {
                                        y: request.analytics.abandoned.reachedFooter,
                                        name: "Fully Read the Content"
                                    },
                                    {
                                        y: request.analytics.abandoned.notReachedFooter,
                                        name: "Skipped the Content"
                                    },
                                ]
                            }
                        ]
                    });
                chart.render();
            };

        SmartBar.prototype.PopupContinuedChart = function (request) {
            var totalBadge = document.querySelector('.continued-sessions-badge-total');
            var reachedBadge = document.querySelector('.continued-sessions-badge-reached');
            var skippedBadge = document.querySelector('.continued-sessions-badge-skipped');

            totalBadge.innerHTML = request.analytics.continued.count;
            reachedBadge.innerHTML = request.analytics.continued.reachedFooter;
            skippedBadge.innerHTML = request.analytics.continued.notReachedFooter;

            console.log(request.analytics);


            if(request.analytics.continued.reachedFooter === 0 && request.analytics.continued.notReachedFooter === 0){
                document.getElementById('continued-sessions-chart').innerHTML = 'No analytics for retention sessions';
                return
            }
            var chart = new CanvasJS.Chart("continued-sessions-chart",
                {
                    theme: "theme1",
                    data: [
                        {
                            type: "doughnut",
                            showInLegend: true,
                            toolTipContent: '{name}',
                            legendText: "{name}",
                            dataPoints: [
                                {
                                    y: request.analytics.continued.reachedFooter,
                                    name: "Fully Read the Content"
                                },
                                {
                                    y: request.analytics.continued.notReachedFooter,
                                    name: "Skipped the Content"
                                },
                            ]
                        }
                    ]
                });
            chart.render();
        };

        SmartBar.prototype.init = function () {
            this.PopupHandleEditMode();
            this.PopupOnLoadListeners();
            this.PopupFormValidate();
            this.PopupChangeAppKey();
            this.PopupFetchPendingExperiments();
            this.PopupCreateNewExperiment();
            this.PopupPreviewExperiment();
            this.PopupEditExperiment();
            this.PopupDeleteExperiment();
            this.PopupFetchAnalytics();
        };

    window.sb = new SmartBar();
    window.sb.init();

})();
