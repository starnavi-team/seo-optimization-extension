var smartbar = (function() {
    function SmartBar() {
        this.elements = [];
        this.selectedElements = [];
        this.selectedElement = '';
        this.editMode = false;
        this.popup = null;
        this.experimentId = null;
        this.experimentName = '';
        this.storage = chrome.storage;
        this.serverKey = '';
    }

    var BASE_URL = 'http://ec2-34-215-210-99.us-west-2.compute.amazonaws.com';



    SmartBar.prototype.EXPERIMENT_STATUS = {
        CREATED: 'created',
        ALL: "all"
    };

    SmartBar.prototype.apiUrls = {
        experiment: BASE_URL + '/api/experiment',
        deleteExperiment: BASE_URL + '/api/experiment/delete',
        element: BASE_URL + '/api/element',
        experimentAll: BASE_URL + '/api/experiment/all',
        experimentByUser: BASE_URL + '/api/user/experiment/fetch',
        analyticsFetch: BASE_URL + '/api/analytics/page',
        analyticsActions: BASE_URL + '/api/analytics/app/save'
    };

    SmartBar.prototype.requestData = function(url, body, callback) {

        // FOR SERVER REQUEST

        var API_KEY = atob(this.serverKey);
        var request = new XMLHttpRequest();
        request.open('POST', url, true);
        request.setRequestHeader('Content-Type', 'application/json');
        request.setRequestHeader('x-app-key', API_KEY);
        request.onload = function() {
            if (callback) {
                callback(JSON.parse(request.response));
            }
        };

        request.send(JSON.stringify(body));
    };

    SmartBar.prototype.ContentGetCssSelector = function(element) {

        // FOR FETCHING CURRENT CSS SELECTOR

        var rightArrowParents = [],
            elm,
            entry;

        for (elm = element; elm; elm = elm.parentNode) {
            entry = elm.tagName.toLowerCase();
            if (entry === "html") {
                break;
            }

            if (elm.id && elm.className) {
                entry += "#"+ elm.id.replace(/ /g, '.') + "." + elm.className.replace(/ /g, '.');
            }

            else if (elm.id) {
                entry += "#" + elm.id.replace(/ /g, '.');
            }

            else if (elm.className) {
                entry += "." + elm.className.replace(/ /g, '.');
            }

            rightArrowParents.push(entry);
        }
        rightArrowParents.reverse();
        return rightArrowParents.join(" ");
    };

    SmartBar.prototype.ContentHighlightElement = function (event) {
        if (!this.editMode) {
            return;
        }

        var elementClass = event.target.className;

        if (elementClass.includes('sb-popup') || elementClass.includes('sb-button') || elementClass.includes('pika') || event.target.id.includes('exp_current')) {
            return;
        }

        event.preventDefault();
        event.stopPropagation();

        var selected = event.target;
        var cssSelector = this.ContentGetCssSelector(selected);

        var selectedElement =  document.querySelector(cssSelector);

        var initialContent = '';
            initialContent = selected.text || selected.innerHTML;
            if(initialContent.includes('class="')) {
                return ;
            }
            if(initialContent === ''){
                return
            }


        selectedElement.addEventListener('mouseover', function () {
            this.style.border = '2px yellow solid'
        });

        selectedElement.addEventListener('mouseout', function () {
            this.style.border = 0;
        });
    };

    SmartBar.prototype.ContentSelectElement = function(event) {

        // FOR FETCHING EDIT MODE. IF TRUE, MAKE POPUP FOR CREATING ELEMENT

        if (!this.editMode || !this.experimentName) return;

        var elementClass = event.target.className;

        if (elementClass.includes('sb-popup') || elementClass.includes('sb-button') || elementClass.includes('pika') || event.target.id.includes('exp_current')) {
            return;
        }

        event.preventDefault();
        event.stopPropagation();

        var selected = event.target;
        var cssSelector = this.ContentGetCssSelector(selected);

        var selectedElement =  document.querySelector(cssSelector);
        this.selectedElements.push(selectedElement);
        this.selectedElement = selectedElement;

        var initialContent = '';
        var elementIndex = this.elements.findIndex(function (item) {
            return item.cssSelector === cssSelector;
        });
        if (elementIndex === -1) {
            initialContent = selected.text || selected.innerHTML;
            if(initialContent.includes('class="')) {
                return ;
            }
            if(initialContent === ''){
                alert('No content to change');
                this.ContentHidePopup();
                return
            }
            this.popup.querySelector('.sb-popup__content').value = initialContent;
        } else {
            initialContent = this.elements[elementIndex].initialValue;
            if(initialContent.includes('class="')) {
                return ;
            }
            this.popup.querySelector('.sb-popup__content').value = this.elements[elementIndex].newValue;
        }

        this.popup.querySelector('.sb-popup__save').onclick = (function () {
            this.ContentSavePopup(selected, cssSelector, initialContent);
        }).bind(this);
        this.ContentShowPopup(event);
    };

    SmartBar.prototype.ContentSavePopup = function(selectedElement, cssSelector, initialContent) {

        // FOR FETCHING ELEMENT CONTENT AND SEND IT TO SERVER FOR CURRENT EXPERIMENT

        this.selectedElement.style.WebkitTransition = 'all 0.3s ease-in-out';

        this.selectedElement.style.color = 'green';
        var that = this;
        setTimeout(function() {
            that.selectedElement.style.color = 'inherit';
        }, 400);


        var newContent = this.popup.querySelector('[name="sb-popup__content"]').value;

        var elementIndex = this.elements.findIndex(function (item) {
            return item.css_selector === cssSelector;
        });

        var callback = function (response) {
            this.elements = response.elements;
        };

        if (elementIndex === -1) {

            this.requestData(this.apiUrls.element, {
                experimentId: this.experimentId,
                elementId: cssSelector,
                path: location.href.replace(/https?:\/\//i, ""),
                initialValue: initialContent,
                newValue: newContent,
                cssSelector: cssSelector
            }, callback.bind(this));


        } else {
            this.elements[elementIndex].newValue = newContent;

            this.requestData(this.apiUrls.element, {
                experimentId: this.experimentId,
                elementId: cssSelector,
                path: location.href.replace(/https?:\/\//i, ""),
                initialValue: initialContent,
                newValue: newContent,
                cssSelector: cssSelector
            }, callback.bind(this));
        }

        if (selectedElement.text) {
            selectedElement.text = newContent;

        } else if (selectedElement.innerHTML) {
            selectedElement.innerHTML = newContent;
        }

        this.ContentHidePopup();
    };

    SmartBar.prototype.ContentCancelPopup = function() {

        // THAT`S ALL, ACTUALLY :) FOR HIDING POPUP

        this.ContentHidePopup();
    };

    SmartBar.prototype.ContentShowPopup = function(event) {

        //FOR GETTING POSITION OF SELECTED ELEMENT AND MAKING POPUP RIGHT NEAR

        this.popup.style.display = 'block';

        var popupTop = event.pageY - 175;
        var popupLeft = event.pageX;

        if ((event.pageX + this.popup.offsetWidth) > window.innerWidth) {
            this.popup.classList.remove('sb-popup--l');
            this.popup.classList.add('sb-popup--r');
            popupLeft -= this.popup.offsetWidth - 5;
        } else {
            this.popup.classList.remove('sb-popup--r');
            this.popup.classList.add('sb-popup--l');
            popupLeft -= 45;
        }

        if ((event.clientY - this.popup.offsetHeight) < 0) {
            this.popup.classList.add('sb-popup--t');
            this.popup.classList.remove('sb-popup--b');
            popupTop += 165;
        } else {
            this.popup.classList.add('sb-popup--b');
            this.popup.classList.remove('sb-popup--t');
            popupTop -= 15;
        }

        this.popup.style.top = popupTop + 'px';
        this.popup.style.left = popupLeft + 'px';

        var currentExperiment = document.getElementById('exp_current');

        currentExperiment.innerHTML = 'Added to: ' + this.experimentName;
    };



    SmartBar.prototype.ContentHidePopup = function() {

        // GUESS WHAT FOR IS IT :)
        this.popup.style.display = 'none';
    };


    SmartBar.prototype.ContentCreateExperiment = function () {

        // FOR FETCHING NEW EXPERIMENT DATA FROM POPUP AND SEND IT TO SERVER FOR CREATING

        var that = this;
        var callback = function(response) {
            that.experimentId = response._id;
            that.experimentName = response.name;
            var callback = function (response) {
                if (response.length > 0) {
                    response.forEach(function(item) {
                        if (item) {
                            if (new Date() < new Date(item.end_time)) {
                                that.experimentName = item.name;
                                that.experimentId = item._id;
                                chrome.runtime.sendMessage({pendingExperiment: item});
                            }
                        }
                    });
                } else {
                    chrome.runtime.sendMessage({pendingExperiment: 'false'});
                }
            };

            that.requestData(that.apiUrls.experimentByUser, {}, callback.bind(this));
            that.ContentSendActionAnalytics('create experiment', response._id)
        };

        chrome.runtime.onMessage.addListener(
            function(request, sender, sendResponse) {
                if(request.exp_name){
                    that.requestData(that.apiUrls.experiment, {
                        name: request.exp_name,
                        startTime: new Date(request.from_date),
                        endTime: new Date(request.to_date),
                        status: that.EXPERIMENT_STATUS.CREATED,
                        createdBy: 'bogdan'
                    }, callback.bind(this));
                }
            }
        );
    };

    SmartBar.prototype.ContentEditExperiment = function () {

        // FOR FETCHING EDIT EXPERIMENT DATA FROM POPUP AND SEND IT TO SERVER FOR EDITING

        var that = this;

        var callback = function(response) {
            console.log(response);
                    if (new Date() < new Date(response.end_time)) {
                        that.experimentName = response.name;
                        that.experimentId = response._id;
                        chrome.runtime.sendMessage({pendingExperiment: response});
                    }
            that.experimentId = response._id;
            that.experimentName = response.name;
            that.ContentSendActionAnalytics('edit experiment', response._id);
        };

        chrome.runtime.onMessage.addListener(
            function(request, sender, sendResponse) {
                if (request.update_exp_name) {
                    that.requestData(that.apiUrls.experiment, {
                        experimentId: request.update_exp_ID,
                        name: request.update_exp_name,
                        startTime: new Date(request.update_from_date),
                        endTime: new Date(request.update_to_date),
                        status: that.EXPERIMENT_STATUS.CREATED,
                        createdBy: 'bogdan'
                    }, callback.bind(this));
                }
            }
        );
    };

    SmartBar.prototype.ContentDeleteExperiment = function () {

        // FOR FETCHING DELETE EXPERIMENT DATA FROM POPUP AND SEND IT TO SERVER FOR DELETING

        var that = this;
        chrome.runtime.onMessage.addListener(
            function(request, sender, sendResponse) {
                if (request.delete_experiment_ID) {
                    that.experimentName = '';
                    that.requestData(that.apiUrls.deleteExperiment, {
                        experimentId: request.delete_experiment_ID,
                    });
                }
            }
        );
    };

    SmartBar.prototype.ContentPreviewExperiment = function () {

        // FOR FETCHING EXPERIMRENT THAT USER WANTS TO PREVIEW AND SEND IT TO FUNCTION THAT ACTIVED IT

        var that = this;
        chrome.runtime.onMessage.addListener(
            function(request, sender, sendResponse) {
                if (request.preview_exp_ID) {
                    that.ContentActiveExperiment(request.preview_exp_ID);
                }
            }
        );
    };

    SmartBar.prototype.ContentFetchEditMode = function () {

        // LISTENING FOR CHANGES ON POPUP SIDE AND SET ENABLE/DISABLE EDIT MODE. FOR CREATING POPUP

        var that = this;

        chrome.runtime.onMessage.addListener(
            function(request, sender, sendResponse) {
                if(request.edit_mode) {
                    if (request.edit_mode === "on") {
                        that.editMode = true;
                    } else if (request.edit_mode === "off") {
                        that.editMode = false;
                    }
                }
            }
        );
    };

    // SmartBar.prototype.ContentSendCurrentExperiment = function () {
    //
    //     // FOR SENDING CURRENT EXPERIMENT TO POPUP
    //
    //     var that = this;
    //     chrome.runtime.onMessage.addListener(
    //         function(request, sender, sendResponse) {
    //             if(request.need_button){
    //                 if(that.experimentName !== ''){
    //                     that.editMode = false;
    //                     sendResponse({should_show_button: "yes", current_experiment: that.experimentName});
    //                 }
    //             }
    //         }
    //     );
    //
    // };

    SmartBar.prototype.ContentCreatePopup = function() {

        // FOR CREATING POPUP

        var container = document.createElement('div');
        container.classList.add('sb-popup');
        container.setAttribute("style", "z-index:214748364; margin:20px; display:none; position:absolute; top:0; left:0; width:300px;  height: 210px; padding:10px; background-color:#58a7d4; border-radius:5px; box-shadow:0 7px 11px -1px rgba(45,87,110,.7)");

        var contentInput = document.createElement('textarea');
        contentInput.classList.add('sb-popup__content');
        contentInput.setAttribute("style", "font-size:0.9rem; margin:0; padding:10px; display:block; min-width:280px; min-height:100px; max-height:100px; border:none; font-family:sans-serif");
        contentInput.name = 'sb-popup__content';
        contentInput.onkeydown = this.ContentHandleEnterKeyDown.bind(this);

        var saveButton = document.createElement('div');
        saveButton.classList.add('sb-popup__save');
        saveButton.setAttribute("style", "text-align:center; color:#222; display:inline-block; padding:5px 15px; font-size:0.9rem; margin-top:10px; margin-right:10px; background-color:#fff; border:none; border-radius:2px; cursor:pointer;");
        saveButton.innerHTML = 'Save';

        var cancelButton = document.createElement('div');
        cancelButton.classList.add('sb-popup__cancel');
        cancelButton.setAttribute("style", "text-align:center; color:#222; display:inline-block; padding:5px 15px; font-size:0.9rem; margin-top:10px; margin-right:10px; background-color:#fff; border:none; border-radius:2px; cursor:pointer;");
        cancelButton.innerHTML = 'Cancel';
        cancelButton.addEventListener('click', this.ContentCancelPopup.bind(this));

        container.appendChild(contentInput);
        container.appendChild(saveButton);
        container.appendChild(cancelButton);

        var currentExperiment = document.createElement('p');
        currentExperiment.id = "exp_current";
        currentExperiment.style.position = "absolute";
        currentExperiment.style.bottom = '0px';
        currentExperiment.style.left = '10px';
        currentExperiment.style.color = "white";


        container.appendChild(currentExperiment);

        this.popup = container;

        document.body.appendChild(this.popup);
    };

    SmartBar.prototype.ContentHandleEnterKeyDown = function(e) {

        // FOR CREATING <br> ELEMENT IF USER TYPE ENTER

        setTimeout(function () {
            if (e.which === 13) {
                var value = e.target.value.split('');
                var offset = e.target.selectionStart;
                var br = '</br>\n';

                value.splice(offset - 1, 1, br);
                e.target.value = value.join('');

                var newOffset = offset + br.length - 1;
                e.target.selectionStart = newOffset;
                e.target.selectionEnd = newOffset;
            }
        }, 0);
    };

    SmartBar.prototype.ContentSetBodyClickHandler = function() {

        document.body.addEventListener('click', this.ContentSelectElement.bind(this));
        document.body.addEventListener('mouseover', this.ContentHighlightElement.bind(this));

    };

    SmartBar.prototype.ContentFetchPendingExperiments = function (cb) {

        // FOR FETCHING ALL FUTURE EXPERIMENTS

        var callback = function (response) {
            var that = this;
            if (response.length > 0) {
                response.forEach(function(item) {
                    if (item) {
                        if (new Date() > new Date(item.end_time)) {
                                chrome.runtime.sendMessage({completedExperiments: item});
                        } else {
                            that.experimentName = item.name;
                            that.experimentId = item._id;
                            chrome.runtime.sendMessage({pendingExperiment: item});
                        }
                    }
                });
                if(cb){
                    cb();
                }
            } else {
                chrome.runtime.sendMessage({pendingExperiment: 'false'});
            }
        };

        this.requestData(this.apiUrls.experimentByUser, {

        }, callback.bind(this));
    };

    SmartBar.prototype.ContentActiveExperiment = function (id) {

        // PART OF PREVIEW ACTION. FETCH ONLY ONE EXPERIMENT THAT USER WANTS TO

        var that = this;
        var callback = function (response) {
            if (response.length) {
                var self = this;
                response.forEach(function(item) {
                    if(item._id === id) {
                        that.experimentId = item._id;
                        that.experimentName = item.name;
                        if (item.elements.length) {
                            self.elements.concat(item.elements);
                            item.elements.forEach(function (element) {
                                if (element.css_selector) {
                                    var contentElement = document.querySelector(element.css_selector);
                                    if (contentElement) {
                                        contentElement.style.WebkitTransition = 'all 0.4s ease-in-out';
                                        contentElement.style.backgroundColor = 'rgba(0, 255, 0, 0.2)';
                                        setTimeout(function() {
                                            contentElement.style.backgroundColor = 'inherit';
                                        }, 700);
                                        contentElement.innerHTML = element.new_value;
                                    }
                                }
                            });
                        }
                    }
                });
            }
        };

        this.requestData(this.apiUrls.experimentByUser, {}, callback.bind(this));
    };

    SmartBar.prototype.ContentSendAnalytics = function () {
        var that = this;
        chrome.runtime.onMessage.addListener(
            function(request, sender, sendResponse) {
                if (request.fetch_analytics) {
                        that.ContentFetchAnalytics();
                };
            }
        );
    };

    SmartBar.prototype.ContentChangeKey = function () {
        chrome.runtime.onMessage.addListener(
            function(request, sender, sendResponse) {
                if (request.change_key) {
                    localStorage.removeItem('smartbar_is_auth');
                }
            }
        );
    };

    SmartBar.prototype.ContentSendActionAnalytics = function(action, experimentID) {

        var generateSessionId = function() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        };

        var sessionId;
        if(!sessionStorage.getItem('session_id')) {
            sessionStorage.setItem('session_id', generateSessionId());
            sessionId = sessionStorage.getItem('session_id');
        } else {
            sessionId = sessionStorage.getItem('session_id');
        }

        var callback = function (response) {
            console.log(response);
        };

        this.requestData(this.apiUrls.analyticsActions, {
            url: location.href.replace(/https?:\/\//i, ""),
            sessionId: sessionId,
            action: action,
            data: { experimentId: experimentID }
        }, callback.bind(this));

    };

    SmartBar.prototype.ContentFetchAnalytics = function () {
        var that = this;
        var today = new Date();
        var yesterday = new Date();
        yesterday.setDate((today.getDate() - 1));
        var callback = function (response) {
            chrome.runtime.sendMessage({analytics: response});
        };

        this.requestData(this.apiUrls.analyticsFetch, {
            url: location.href.replace(/https?:\/\//i, ""),
            startTime: yesterday,
            endTime: today
        }, callback.bind(this));
    };

    SmartBar.prototype.ContentInitialFetch = function() {

        var that = this;

        setTimeout(function () {
            document.body.style.display === 'none' ? document.body.style.display = 'block' : '';
        }, 2000);
        chrome.runtime.sendMessage({page_load: 'yes'});


        // FOR ALL INFORMATION THAT MUST BE FETCHING FIRST: AUTHENTICATION, WIDGET ON PAGE

        function receiveMessage(event) {
            if(event.data === 'success') {
                chrome.runtime.sendMessage({to_show: true});
            }
            if(event.data.page_key){
                that.serverKey = event.data.page_key;
            }
        }
        window.addEventListener("message", receiveMessage, false);

        chrome.runtime.onMessage.addListener(
            function(request, sender, sendResponse) {
                if (request.background_key){
                    if (request.background_key  === 'valid') {
                        that.ContentFetchPendingExperiments();
                    }
                }

                else if (request.is_auth === 'yes') {
                    that.ContentFetchPendingExperiments();
                }

                else if (request.need_show) {
                    window.postMessage({need_show: "true"}, "*");
                }

                else if (request.need_auth_key) {
                    if(localStorage.getItem('smartbar_is_auth')){
                        chrome.runtime.sendMessage({is_auth: true});
                        that.ContentFetchPendingExperiments();
                    }
                }

                else if (request.content_valid_key) {
                    if(request.content_valid_key === atob(that.serverKey)) {
                        sendResponse({compare: 'true'});
                        localStorage.setItem('smartbar_is_auth', 'true');
                    } else {
                        sendResponse({compare: 'false'});
                    }
                }
            }
        );
    };

        return {
        init: function initSmartBar(domain, path) {
            this.domain = domain;
            this.path = path;

            var smartBar = new SmartBar();
            smartBar.ContentInitialFetch();
            smartBar.ContentCreatePopup();
            smartBar.ContentSetBodyClickHandler();
            smartBar.ContentChangeKey();
            smartBar.ContentCreateExperiment();
            smartBar.ContentPreviewExperiment();
            smartBar.ContentEditExperiment();
            smartBar.ContentFetchEditMode();
            smartBar.ContentDeleteExperiment();
            // smartBar.ContentSendCurrentExperiment();
            smartBar.ContentSendAnalytics();
        },
    };
}());


window.smartbar = smartbar;

window.smartbar.init();
